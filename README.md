# Flask Template

## Availability

### Clone it from GitLab

```bash
git clone https://gitlab.com/sayalg/flask-template.git
```

### Homepage

The flask-template project is available at https://gitlab.com/sayalg/flask-template.

### Contact the author

You can write an email to Mr. Sayal Guirales at sguirale[at]uncc.edu.

## Dependencies

* Python v3+
* Flask v0.10.2+
* Flask-SQLAlchemy v1.+
* Flask-WTF v0.9.3 or superior

This project was designed for macOS or Linux computers.

## How to run

Move into the working directory (the same where `my-app.py` is located) and execute the following command line:

```bash
$ bash execute_flask.sh
```

Once the Flask application is running, you should be able to see your app at http://127.0.0.1:5000/ in your favorite browser.