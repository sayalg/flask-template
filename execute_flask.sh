#!/usr/bin/env bash

# App name: execute_flask.sh
# Usage: bash execute_flask.sh

##
# Define functions
##

function change_mod {
	chmod +rx-w my-app.py
}

function run_flask {
	FLASK_APP=my-app.py flask run
	wait
}

##
# Execute functions
##

change_mod
run_flask

exit
